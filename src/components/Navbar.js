import React from 'react'
import "./Navbar.css"

 function Navbar() {
  return (
    <nav>
<img className='image7' src="./images/image7.png" alt=""/>
<ul>

    <li><a className='active' href>MAIN</a></li>
    <li><a className='active' href="#">GLITCH PORTAL</a></li>
    <li><a className='active' href="#">PROJECTS</a></li>
    <li><a className='active' href="#">ROADMAP</a></li>
    <li><a className='active' href="#">TEAM</a></li>
    <li><a className='active' href="#">CONTACT</a></li>
</ul>
    </nav>
  );
}

export default Navbar;